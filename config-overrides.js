const { override, fixBabelImports, addLessLoader, useBabelRc, addWebpackAlias } = require('customize-cra');
const path = require('path')
module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: { 'primary-color': '#54C3D4' },
    }),
    useBabelRc(),
    addWebpackAlias({
        Message: path.resolve(__dirname, 'src/components/message/message.js'),
        Service: path.resolve(__dirname, 'src/service'),
        Redux: path.resolve(__dirname, 'src/redux'),
        Containers: path.resolve(__dirname, 'src/containers'),
        Components: path.resolve(__dirname, 'src/components'),
        Config: path.resolve(__dirname, 'src/config')
    }),
);