import React, { PureComponent } from 'react';
import './worksShow.less';
import Worksshowitem from 'Components/Works/worksShowItem.js'
import { connect } from 'react-redux';
import { Row, Col, Card, Pagination } from 'antd';
import { getWorkList } from 'Redux/actions/worksactions.js';
@connect(({ works: { selectedWorks, netWorks } }) => ({ selectedWorks, netWorks }))
class Worksshow extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            newPageNumber: 0,
            newPageSize: 12,
            selectedPageNumber: 0,
            selectedPageSize: 12
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { newPageNumber, newPageSize, selectedPageNumber, selectedPageSize } = this.state;
        dispatch(getWorkList({ dataType: '1',type: '1', pageNumber: selectedPageNumber, pageSize: selectedPageSize }))
        dispatch(getWorkList({ dataType: '4',pageNumber: newPageNumber, pageSize: newPageSize }))
    }
    onChangeNewPage = (page) => {
        const { dispatch } = this.props;
        const { newPageSize: pageSize } = this.state;
        const postdata = {
            dataType: '3',
            type: '3',
            pageNumber: page - 1,
            pageSize
        }
        dispatch(getWorkList(postdata))
        this.setState({ newPageNumber: page - 1 })
    }
    // onChangSelectedPage = (page) => {
    //     const { dispatch } = this.props;
    //     const { selectedPageSize: pageSize } = this.state;
    //     const postdata = {
    //         type: '3',
    //         pageNumber: page - 1,
    //         pageSize
    //     }
    //     dispatch(getWorkList(postdata))
    //     this.setState({ selectedPageNumber: page - 1 })
    // }
    render() {
        const { selectedWorks, netWorks } = this.props;
        const { content: selectedWorksList = [] } = selectedWorks;
        const { content: newWorksList = [], pageable: newPageable = {} } = netWorks;
        const { totalSize: newTotalSize = 12 } = newPageable;
        const { newPageNumber, newPageSize } = this.state;
        return (
            <div className='works-show'>
                <Card style={{ borderRadius: '10px' }}>
                    {/* 精选作品 */}
                    <Row type='flex' style={{ flexDirection: 'column' }} >
                        <Row className='works-show-row'>
                            <img
                                style={{ width: '29px', height: '25px', marginRight: '12px' }}
                                src={`${process.env.PUBLIC_URL}/assets/icon-imperial-crown.png`} alt='' />精选作品</Row>
                        <Row>
                            {selectedWorksList.map(item => {
                                return <Col key={item.id}
                                    style={{
                                        minWidth: '286px',
                                        marginBottom: '26px',
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }} span={6}>
                                    <Worksshowitem detail={item} />
                                </Col>
                            })
                            }</Row>
                    </Row>
                    {/* 最新作品 */}
                    <Row type='flex' style={{ flexDirection: 'column' }} >
                        <Row className='works-show-row'>
                            <img
                                style={{ width: '27px', height: '27px', marginRight: '12px' }}
                                src={`${process.env.PUBLIC_URL}/assets/icon-new.png`} alt='' />最新作品</Row>
                        <Row>
                            {newWorksList.map(item => {
                                return <Col key={item.id}
                                    style={{
                                        minWidth: '286px',
                                        marginBottom: '26px',
                                        display: 'flex',
                                        justifyContent: 'center'
                                    }} span={6}>
                                    <Worksshowitem detail={item} />
                                </Col>
                            })
                            }
                        </Row>
                        <div className='works-show-notification'>
                            <Pagination
                                current={newPageNumber + 1}
                                pageSize={newPageSize}
                                onChange={this.onChangeNewPage}
                                hideOnSinglePage={true}
                                defaultCurrent={1}
                                total={newTotalSize} />
                        </div>
                    </Row>
                </Card>
            </div>
        )
    }
}
export default Worksshow;