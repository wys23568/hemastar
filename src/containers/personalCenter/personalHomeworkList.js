import React, { PureComponent } from 'react';
import './personalHomeworkList.less';
import { connect } from 'react-redux';
import PersonalCenterNav from 'Components/personalCenter/personalCenterNav.js';
import { Scratch } from '../myCourse.js';
import { Row, Col, Icon } from 'antd';
import * as message from 'Message';
import classNames from 'classnames';
import { getCourseHomeworkList } from 'Redux/actions/personalactions.js';

@connect(({ login: { userInfo }, personal: { courseHomeworkList } }) => ({ userInfo, courseHomeworkList }))
class PersonalHomeworkList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pageNumber: 0,
            pageSize: 20,
            visible: false
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const { pageNumber, pageSize } = this.state;
        dispatch(getCourseHomeworkList({ pageNumber, pageSize }))
    }
    showModal = () => {
        document.body.style.overflow = 'hidden'
        this.setState({
            visible: true,
        });
    }
    handleCancel = () => {
        document.body.style.overflow = 'auto'
        this.setState({
            visible: false,
        });
    }
    onClickToCourse = (item) => {
        const { history } = this.props;
        if (item.buyFlag === 2) {
            if (item.courseInfo.state > 4) {
                history.push(`/personal-center/my-homework/${item.courseInfo.id}`)
            }
            else {
                message.info('此课程尚未开课，请再等待一下吧~', 1)
            }
        }
        else {
            this.showModal()
        }
    }
    render() {
        const { courseHomeworkList, userInfo } = this.props;
        const { coursePackageInfoList = [] } = courseHomeworkList;
        const { visible } = this.state;
        return (
            <div className='series'>
                <div className='series-left'>
                    <PersonalCenterNav userInfo={userInfo} />
                </div>
                <div className='series-right'>
                    {<Scratch
                        style={{
                            width: '124px',
                            paddingBottom: '15px',
                            borderBottom: '3px solid #54C3D4',
                            color: '#54C3D4'
                        }} >
                        <img
                            style={{ width: '22px', height: '22px', marginRight: '9px' }}
                            src={`${process.env.PUBLIC_URL}/assets/icon-scratch-lesson.png`}
                            alt='' />Scratch课程</Scratch>}
                    <Row
                        type='flex'
                        justify='start'
                        className='series-right-content' >
                        {
                            coursePackageInfoList.map((item, index) => {
                                return <Col
                                    className='series-right-content-item'
                                    key={item.id}
                                    span={6}>
                                    <div
                                        style={{
                                            background: `url(${item.coverUrl}) no-repeat top`,
                                            backgroundSize: 'cover'
                                        }}
                                        onClick={this.onClickToCourse.bind(this, item)}
                                        className={classNames({ 'series-right-content-item-course': true, gray: item.buyFlag !== 2 })}>
                                        <div style={{ fontSize: '14px', color: '#3F3F3F' }}>{item.lessonCount}课时</div>
                                        <div style={{ fontSize: '20px', color: '#3F3F3F', padding: '3px 0 10px 0' }}>{item.name}</div>
                                        <div style={{ fontSize: '26px', color: '#FFFFFF' }}>LV{index}</div>
                                    </div>
                                </Col>
                            })
                        }
                    </Row>
                </div>
                {
                    visible && <div onClick={this.handleCancel} className='series-modal'>
                        <div
                            onClick={(e) => { e.stopPropagation() }}
                            className='series-modal-content'
                            style={{
                                background: `url(${process.env.PUBLIC_URL}/assets/gbg/gbg-message.png) no-repeat top`,
                                backgroundSize: 'cover'
                            }}>
                            <Icon
                                onClick={this.handleCancel}
                                type="close-circle"
                                style={{ fontSize: '51px', color: '#c0bfbc' }} />
                        </div>
                    </div>
                }
            </div>
        )
    }
}
export default PersonalHomeworkList;