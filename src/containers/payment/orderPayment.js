import React from 'react';
import './orderPayment.less';
import { Button, Radio, Icon, Modal } from 'antd';
import { connect } from 'react-redux';
import { createOrder, showOrClosePayModal, getOrderDetail, continuePaying } from 'Redux/actions/paymentactions.js';
import { confirmOrderStatus } from 'Service/payment.js';
import QRCode from 'qrcode.react';
import moment from 'moment';
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
@connect(({ login: { token }, payment: { payVisible, orderInfo, orderDetail } }) => ({ token, payVisible, orderInfo, orderDetail }))
class Orderpayment extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            payChannel: '1',
            time: 180,
            isRefresh: false
        }
    }
    componentDidMount() {
        const { dispatch, token, match: { params }, history } = this.props;
        dispatch(getOrderDetail({ courseId: params.cid }));
        this.payer = setInterval(() => {
            const { orderInfo } = this.props;
            const { orderNo } = orderInfo;
            confirmOrderStatus({ orderNo, loginToken: token }).then(function (response) {
                if (response.data.code !== 200) {
                    return
                }
                if (response.data.data && response.data.data.state === 3) {
                    history.push(`/order-payment/${params.cid}/${orderNo}`)
                }
            })
                .catch(function (error) {
                    console.log(error);
                });

        }, 3000)
        this.timer = setInterval(() => {
            this.setState(preState => {
                if (preState.time === 1) {
                    return {
                        ...preState,
                        isRefresh: true,
                    }
                }
                else {
                    return {
                        ...preState,
                        time: preState.time - 1
                    }
                }
            })
        }, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.timer);
        clearInterval(this.payer);
    }
    onChangePayment = (e) => {
        this.setState({ payChannel: e.target.value })
    }
    onClickPayment = () => {
        const { dispatch, match: { params }, location } = this.props;
        const { payChannel } = this.state;
        const payTypeList = new Map([['1', 'weixin-web'], ['2', 'weixin-web']])
        let urlParams = new URLSearchParams(location.search);
        urlParams.get('orderNo') ?
            dispatch(continuePaying({ courseId: params.cid, orderNo: urlParams.get('orderNo'), payChannel, payType: payTypeList.get(payChannel) }))
            :
            dispatch(createOrder({ courseId: params.cid, payChannel, payType: payTypeList.get(payChannel) }))
        this.setState({ time: 180 })
    }
    handleCancel = () => {
        const { dispatch } = this.props;
        dispatch(showOrClosePayModal(false))
    }
    onClickRefresh = () => {
        const { dispatch, match: { params }, location } = this.props;
        const { payChannel } = this.state;
        const payTypeList = new Map([['1', 'weixin-web'], ['2', 'weixin-web']])
        let urlParams = new URLSearchParams(location.search);
        urlParams.get('orderNo') ?
            dispatch(continuePaying({ courseId: params.cid, orderNo: urlParams.get('orderNo'), payChannel, payType: payTypeList.get(payChannel) }))
            :
            dispatch(createOrder({ courseId: params.cid, payChannel, payType: payTypeList.get(payChannel) }))
        this.setState({ isRefresh: false, time: 180 })
    }
    render() {
        const { payVisible, orderInfo, orderDetail } = this.props;
        const { payResult = {} } = orderInfo;
        const { time, isRefresh } = this.state;
        return <div className='order-payment'>
            <div style={{ width: '100%', background: '#fff', borderRadius: '10px' }} >
                <div className='order-payment-top'>
                    <div className='order-payment-top-title'>订单信息</div>
                    <div className='order-payment-top-item'>
                        <div className='order-payment-top-item-left'>课程名称：</div>
                        <div style={{ width: '268px', padding: '4px 11px', borderRadius: '5px' }}>{orderDetail.name}</div>
                        {/* <Input
                            value={orderDetail.name}
                            disabled={true}
                            style={{ width: '268px' }}
                        /> */}
                    </div>
                    <div className='order-payment-top-item'>
                        <div className='order-payment-top-item-left'>课程期次：</div>
                        <div style={{ paddingLeft: '11px', borderRadius: '5px' }}>{orderDetail.issue}</div>
                        {/* <Input
                            value={orderDetail.issue}
                            disabled={true}
                            style={{ width: '90px', marginRight: '8px' }}
                        /> */}
                        期
                    </div>
                    <div className='order-payment-top-item'>
                        <div className='order-payment-top-item-left'>课程辅导时间：</div>
                        <div style={{ width: '268px', padding: '4px 11px', borderRadius: '5px' }}>
                            {`${moment(orderDetail.teachStartAt).format('YYYY年MM月DD日')}~${moment(orderDetail.teachEndAt).format('YYYY年MM月DD日')}`}</div>
                        {/* <Input
                            value={`${moment(orderDetail.teachStartAt).format('YYYY年MM月DD日')}~${moment(orderDetail.teachEndAt).format('YYYY年MM月DD日')}`}
                            disabled={true}
                            style={{ width: '268px' }}
                        /> */}
                    </div>
                    <div style={{ padding: '6px 0 0 139px', fontSize: '12px', color: '#999999' }}>在线辅导时间：周一至周五 9:00 - 22:00 </div>
                    <div className='order-payment-top-item'>
                        <div className='order-payment-top-item-left'>支付方式：</div>
                        <RadioGroup defaultValue="1" size="large" onChange={this.onChangePayment}>
                            <RadioButton style={{ marginRight: '10px' }} value='1'>
                                <Icon type="wechat" theme="filled" />{`\xa0微信支付`}</RadioButton>
                            {/* <RadioButton value='2'><Icon type="alipay-circle" theme="filled" />{`\xa0支付宝支付`}</RadioButton> */}
                        </RadioGroup>
                    </div>
                    <div className='order-payment-top-price'>
                        <div className='order-payment-top-price-past'>{`原价：${orderDetail.price}元`}</div>
                        <div className='order-payment-top-price-now'>订单总额：<span style={{ fontSize: '24px', color: '#FF6303' }}>
                            {`${orderDetail.realPrice}元`}</span></div>
                    </div>
                </div>
                <div className='order-payment-bottom'>
                    <div className='order-payment-bottom-tip'>
                        <div style={{ textAlign: 'center' }}>支付后，即可在</div>
                        <div style={{ textAlign: 'center' }}>“我的课程”中查看已购课程。</div>
                    </div>
                    <Button
                        onClick={this.onClickPayment}
                        className='order-payment-bottom-button'
                        type='primary'>立即支付</Button>
                </div>
            </div>
            <Modal
                width={'413px'}
                footer={null}
                visible={payVisible}
                onCancel={this.handleCancel}
                closable={false}
                destroyOnClose={true}
            >
                <div className='pay-modal'>
                    <div className='pay-modal-title'>请用扫一扫完成支付</div>
                    <div className='pay-modal-time'>{isRefresh ? '请点击下图刷新二维码' :
                        <div> 请在<span style={{ color: '#FF6303' }}>{`${time}s`}</span>内完成扫码</div>}</div>
                    <img src={payResult.codeurl} alt='' />
                    <div style={{ position: 'relative', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <QRCode value={payResult.codeurl} size={256} fgColor={isRefresh ? 'rgba(68, 68, 68, 0.5)' : '#000000'} />
                        {isRefresh && <Icon onClick={this.onClickRefresh} className='pay-modal-sync' type="sync" />}
                    </div>
                    <div className='pay-modal-tip'>支付后，即可在“我的课程”中查看已购课程。</div>
                </div>
            </Modal>
        </div>
    }
}
export default Orderpayment;