import React, { PureComponent } from 'react';
import { Icon, Button } from 'antd';
import './paySuccess.less';
import { connect } from 'react-redux';
import { getOrderDistribution } from 'Redux/actions/paymentactions.js';
import Orderdistribution from 'Components/orderDistribution/orderDistribution.js';
@connect(({ payment: { orderDistribution } }) => ({ orderDistribution }))
class Paysuccess extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {
        const { dispatch, match: { params } } = this.props;
        dispatch(getOrderDistribution({ courseId: params.cid }))
    }
    onClickToCourse = () => {
        const { history } = this.props;
        history.push('/my-course')
    }
    render() {
        const { orderDistribution } = this.props;
        const { qrCodeUrl4Official = '', qrCodeUrl4Teacher = '' } = orderDistribution;
        return (
            <div className='pay-success'>
                <div className='pay-success-back'>
                    <Icon
                        onClick={this.onClickToCourse.bind(this)}
                        type="close-circle" theme="filled" />
                </div>
                <div className='pay-success-content'>
                    <div className='pay-success-content-title'>
                        <Icon
                            style={{ fontSize: '60px', color: '#1BB934', marginRight: '11px' }}
                            type="check-circle"
                            theme="filled" />支付成功
                    </div>
                    <div className='pay-success-content-tips'>亲爱的家长，做好以下2步，完成开课前准备哦！</div>
                    <Button
                        onClick={this.onClickToCourse.bind(this)}
                        className='pay-success-content-button'
                        type='default'
                    >查看已购课程</Button>
                    <div className='pay-success-content-p'>
                        < Orderdistribution qrCodeUrl4Official={qrCodeUrl4Official} qrCodeUrl4Teacher={qrCodeUrl4Teacher} />
                    </div>
                </div>
            </div>
        )
    }
}
export default Paysuccess;