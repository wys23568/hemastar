import React from 'react';
import { connect } from 'react-redux';
import * as message from 'Message';
import moment from 'moment';
import styled from 'styled-components';
import classNames from 'classnames';
import { Button, Icon, Carousel, Modal, Select, Input, Anchor } from 'antd';
import Swiper from 'swiper/dist/js/swiper.js'
import 'swiper/dist/css/swiper.min.css'

import HomeVideo from 'Components/homeVideo/homeVideo.js';
import { getWorkList } from 'Redux/actions/worksactions.js'
import { showLoginModal, weixinLogin } from 'Redux/actions/loginactions.js';
import { getHomepageInfo,getUserInfoGather,getCourseDetail } from 'Redux/actions/homepageactions.js';
import configApi from 'Config/configApi.js';
import './homepage.less';
const { Option } = Select;
const { Link } = Anchor;

const ArrowDiv = styled.div`
transition:all 0.5s;
transform:${props => `translateX(${props.arrowRemoveSize}px)`};
`

@connect(({ homepage: { homepageInfo,userInfoGather,homeCourseDetail, loading }, login: { token },works: { goodWorks } }) => ({ homepageInfo,userInfoGather,homeCourseDetail, loading, token, goodWorks }))
class Homepage extends React.PureComponent {
    constructor() {
        super()
        this.state = {
            token: '',
            visible: false,
            lessonInfo: {},
            arrowRemoveSize: 52,
            checkCourse: 1,
            phoneStatus: false,
            nameStatus: false,
            ageStatus: false,
            slideIndex: 0,
            showvideoBox: false,
            showVideo: false,
            showVideoPause: false,
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        const GetQueryString = (name) => {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return (r[2]); return null;
        }
        if (GetQueryString('code')) {
            dispatch(weixinLogin(GetQueryString('code')))
        }
        dispatch(getHomepageInfo())
        dispatch(getCourseDetail())
        dispatch(getWorkList({ dataType: '3',type: '3', pageNumber: '0', pageSize: '10' }))
    }
    componentWillUnmount() {
        document.body.onscroll = null
    }
    componentDidUpdate(prevProps, prevState) {
        if (!prevProps.token && this.props.token) {
            window.location.href = configApi.routerApi;
        }
        const { homepageInfo } = this.props;
        const {teacherInfoList} = homepageInfo;
        if (teacherInfoList) {
            this.slidesLoad()
        }
    }
    slidesLoad = () => {
        new Swiper('.swiper-container',{
            watchSlidesProgress: false,
            slidesPerView: 'auto',
            centeredSlides: true,
            loop: true,
            autoplay: true,
            loopAdditionalSlides: 0,
            noSwipingClass : 'stop-swiping',
            observer:true,//修改swiper自己或子元素时，自动初始化swiper 
            observeSlideChildren:true,

            onSlideChangeEnd: function(swiper){ 
            　　swiper.update();
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            on: {
                init: function () {
                    this.emit('transitionEnd');
                },
                progress: function(progress) {
                    for ( let i = 0;i< this.slides.length; i++) {
                        var slide = this.slides.eq(i);
                        var slideProgress = this.slides[i].progress;
                        if(slideProgress > 0) {
                            slide.transform('translateX(508px) scale(0.73)');
                            slide.css('zIndex', 0);
                            slide.css('opacity', 0.5);
                        } else if (slideProgress < 0) {
                            slide.transform('translateX(-508px) scale(0.73)');
                            slide.css('zIndex', 0);
                            slide.css('opacity', 0.5);
                        } else {
                            slide.transform('translateX(0) scale(1)');
                            slide.css('zIndex', 5);
                            slide.css('opacity', 1);
                        }
                    }
                },
                setTransition: function(transition) {
                    for (var i = 0; i < this.slides.length; i++) {
                        var slide = this.slides.eq(i)
                        slide.transition(transition);
                    }
                }
            },
        })
    }
    onClickToPay = () => {
        const { token, dispatch,homepageInfo } = this.props;
        
        const { coursePackageInfoList} = homepageInfo;
        const course = coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0];
        if (!token) {
            dispatch(showLoginModal(true))
            return
        }
        else if (!course) {
            message.info('课程未开放购买~')
            return
        }
        else if (course.buyFlag === 2) {
            message.info('您已购买过该课程，请不要重复购买呢~')
            return
        }
        this.setState({ visible: true })
    }
    arrowRemove = (course) => {
        this.setState({
            checkCourse: course,
            arrowRemoveSize: 50 + 169*(course - 1)
        })
        this.chooseIndex.goTo(course-1)
    }
    handleCancel = e => {
        this.setState({
            visible: false,
            lessonInfo: {}
        });
    };
    onChangeLesson = (value) => {
        const { homepageInfo } = this.props;
        const { coursePackageInfoList = [] } = homepageInfo;
        let lessonInfo;
        coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList.forEach(item => {
            if (item.id === value.key) {
                lessonInfo = item
            }
        })
        this.setState({ lessonInfo })

    }
    onClickHasChoose = () => {
        const { lessonInfo } = this.state;
        const { history } = this.props;
        if (!lessonInfo.issue) {
            message.info('请您先选择课程期次呢~')
            return
        }
        this.setState({ visible: false })
        history.push(`/order-payment/${lessonInfo.id}`)

    }
    onClickSave = () => {
        const { dispatch } = this.props;
        const { phone, name, age } = this.state;
        if (!(/^1\d{10}$/.test(phone))) {
            this.setState({ phoneStatus: true })
        }
        else if (!name) {
            this.setState({ nameStatus: true })

        }
        else if (!age) {
            this.setState({ ageStatus: true })

        }
        else {
            dispatch(getUserInfoGather({ phone, name, age }))
        }

    }
    toProductDetail = (id) => {
        window.open(`${configApi.routerApi}#/works-show/${id}`)
    }
    // 观看视频
    showVideoFn = (flag) => {
        this.setState({
            showvideoBox: flag,
            showVideo: flag,
        })
    }
    render() {
        const { homepageInfo, userInfoGather, loading, goodWorks,homeCourseDetail } = this.props;
        const { coursePackageLessonInfoList = []} = homeCourseDetail
        const { content: goodWorkList = [] } = goodWorks;
        const { coursePackageInfoList = [], qrCodeUrl4Official = '', teacherInfoList = [] } = homepageInfo;
        const { visible, lessonInfo,arrowRemoveSize,checkCourse, phoneStatus, nameStatus, ageStatus,showVideo,showvideoBox,showVideoPause } = this.state;
        const whyHaimawangPic = [{ id: 1, height: '73px' },{ id: 2, height: '84px' },{ id: 3, height: '105px' },{ id: 4, height: '133px' },{ id: 5, height: '155px' },{ id: 6, height: '175px' }];
        const whyHaimawangWord = [{ id: 1, background: '#54C2FB', content: '熟悉界面和流程培养编程兴趣' },
        { id: 2, background: '#54C2FB', content: '多样化编程效果实现提升自主纠错水平和逻辑思维能力' },
        { id: 3, background: '#7CD702', content: '不同语句综合应用理解分支程序之间的区别和联系' },
        { id: 4, background: '#7CD702', content: '程序自主创作提升制作多角色、多背景的复杂流程程序' },
        { id: 5, background: '#FFBD00', content: '解锁列表、函数等高级模块应用编程和数字艺术相结合' },
        { id: 6, background: '#FFBD00', content: '多学科融合进行编程项目创作，培养孩子解决问题能力' }];
        const testCourseData = [
            {
                id: 0,
                title: '趣味过河',
                codeKnowledge: '熟悉界面，认识程序模块初识顺序结构',
                courseKnowledge: '掌握Scratch界面和流程操作，运用运动模块实现角色简单移动。',
                content: '【数学】坐标和角度'
            },
            {
                id: 1,
                title: '奔跑吧波利',
                codeKnowledge: '1.条件语句的运用 2.初识循环结构',
                courseKnowledge: '了解基本编程指令语句，侦测和程序分支语句同时应用，加深理解。',
                content: '【科学】动画的实现原理',
            },
            {
                id: 2,
                title: '攀岩挑战赛',
                codeKnowledge: '1.条件语句强化 2.新建和使用变量',
                courseKnowledge: '认识变量，学习新建变量，并用变量来编写有趣的程序作品。',
                content: '【数学】变量的概念和应用',
            },
            {
                id: 3,
                title: '迷宫探险',
                codeKnowledge: '1.巩固变量的使用 2.按键操作控制角色移动',
                courseKnowledge: '学习克隆的使用和操作方法，并用变量对其进行修改和升级。',
                content: '【科学】视觉暂留效应',
            },
            {
                id: 4,
                title: '大闹地鼠王宫',
                codeKnowledge: '1.循环和选择结构的叠用 2.参数的随机设置',
                courseKnowledge: '学习随机数的设置，运算符初体验，并在多角色间进行程序迁移。',
                content: '【数学】随机的概念和意义',
            },
            {
                id: 5,
                title: '海底保卫战',
                codeKnowledge: '1.运算符的比较判断 2.角色面向鼠标指针操作',
                courseKnowledge: '选择语句中加入运算符比较，鼠标键盘同时控制不同角色，程序逻辑和计算机操作综合提升。',
                content: '【科学】海底生态环境保护',
            },
        ]
        return (
            <div className='homepage'>
                {/* 太空屏 */}
                <div id="haima" className='homepagebigpicture' style={{background: `url(${process.env.PUBLIC_URL}/assets/picture-background.png) no-repeat top`,backgroundSize: 'auto 1003px',height: '1003px'}}>
                    <div className='homepage-big-picture'>
                        <div className='homepage-big-picture-left'>
                            <img src={`${process.env.PUBLIC_URL}/assets/picture-background-word.png`} style={{ width: '400px' }} alt='' />
                        </div>
                        <div className='homepage-big-picture-middle'>
                            <img src={`${process.env.PUBLIC_URL}/assets/picture-background-vessel.png`} className="vesselImg" alt=''/>
                        </div>
                    </div>
                </div>
                {/* 内容区域 */}
                <div className="homepageMidCon" style={{marginTop: "-200px"}}>
                    {/* 孩子为什么要学编程 */}
                    <div id="code" className='whylearn'>
                        <TitleBox title={'孩子为什么要学编程'} subTitle={'提高核心竞争力'} color={'white'}/>
                        <div className="whylearn-body">
                            <div>
                                <img src={`${process.env.PUBLIC_URL}/assets/whylearn1.png`} alt=''/>
                                <div>锻炼逻辑思维能力</div>
                                <div>抓住成长黄金阶段</div>
                            </div>
                            <div>
                                <img src={`${process.env.PUBLIC_URL}/assets/whylearn2.png`} alt=''/>
                                <div>增强未来竞争力</div>
                                <div>面向人工智能时代</div>
                            </div>
                            <div>
                                <img src={`${process.env.PUBLIC_URL}/assets/whylearn3.png`} alt=''/>
                                <div>提升学习自信力</div>
                                <div>遇到困难勇于尝试</div>
                            </div>
                            <div>
                                <img src={`${process.env.PUBLIC_URL}/assets/whylearn4.png`} alt=''/>
                                <div>开发孩子的想象力</div>
                                <div>让孩子创造无限可能</div>
                            </div>
                        </div>
                    </div>
                    {/* 相关教师 */}
                    <div id="teacher" className="relevantTeachers">
                        <BallAniBox position={{'left': 0, 'top': '-40px'}} bigPosition={{}} smallPosition={{'top': '0','left': '15px'}}></BallAniBox>
                        <div className="relevantTeachers-top" style={{backgroundColor: '#00104D',backgroundImage: `url(${process.env.PUBLIC_URL}/assets/teacherBackground.png)`,backgroundRepeat: 'no-repeat',backgroundSize: '100% 400px', backgroundPosition: '0% 0%'}}>
                            <TitleBox title={'哈工大优秀师资品质保证'} subTitle={'对于课程质量，层层严格把关'} color={'#00104D'}/>
                        </div>
                        <div className="relevantTeachers-center">
                            <div id="certify">
                                <div className="swiper-container">
                                    <div className="swiper-wrapper">
                                    {teacherInfoList ? teacherInfoList.map(item => {
                                            return <div key={item.id}  className="swiper-slide stop-swiping">
                                                <div className="swiper-slide-body">
                                                    <div className="swiper-slide-left">
                                                        <img src={item.avatarUrl} alt='' />
                                                        <div style={{textAlign: 'center',fontSize: '18px'}}>{item.name}</div>
                                                    </div>
                                                    <div className="swiper-slide-right">
                                                        <div style={{fontSize: '18px'}}>老师介绍</div>
                                                        <div className="desc">{item.info}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        }) : ""}
                                        
                                    </div>
                                </div>
                                <div className="swiper-button-prev">
                                    <img style={{width: '19px', height: '34px'}} src={`${process.env.PUBLIC_URL}/assets/arrow.png`} alt="" />
                                </div>
                                <div className="swiper-button-next">
                                    <img style={{width: '19px', height: '34px', transform: 'rotate(-180deg)'}} src={`${process.env.PUBLIC_URL}/assets/arrow.png`} alt="" />
                                </div>
                            </div>


                        </div>
                        <div className="relevantTeachers-bot" style={{backgroundColor: '#fff',backgroundImage: `url(${process.env.PUBLIC_URL}/assets/teacherBackground.png)`,backgroundRepeat: 'no-repeat',backgroundSize: '100% 400px', backgroundPosition: '0% 100%'}}></div>
                    </div>
                    {/* 专业的课程体系 */}
                    <div id="course" className='courseSystem'>
                        <div className='why-haimawang'>
                            <BallFloat style={{width: '20px',height: '20px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', left: '10px',top: '270px'}}></BallFloat>
                            <BallFloat style={{width: '35px',height: '35px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', left: '70px',top: '210px'}}></BallFloat>
                            <BallFloat style={{width: '27px',height: '27px',background: 'linear-gradient(#70A7FE,#4271FF)', left: '160px',top: '320px'}}></BallFloat>
                            <BallFloat style={{width: '47px',height: '47px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', right: '190px',top: '185px'}}></BallFloat>
                            <BallFloat style={{width: '27px',height: '27px',background: 'linear-gradient(#70A7FE,#4271FF)', right: '320px',top: '134px'}}></BallFloat>
                            <TitleBox title={'专业的课程体系'} subTitle={'让孩子循序渐进夯实基础，提升核心竞争力'} color={'#00104D'}/>
                            <div style={{ fontSize: '48px', color: '#54C2FB',textAlign: 'center',marginTop: '25px' }}>科学对标美国CSTA标准</div>
                            <div className='why-haimawang-tags'>
                                <div className='why-haimawang-tags-row'>
                                    <div className='why-haimawang-tags-row-one' style={{ height: '56px', marginRight: '1px' }}>课程阶段</div>
                                    {whyHaimawangPic.map(item => {
                                        return <img
                                            className='why-haimawang-tags-row-one'
                                            style={{ width: '126px', height: item.height }}
                                            key={item.id}
                                            src={`${process.env.PUBLIC_URL}/assets/why-haimawang-lv${item.id}.png`}
                                            alt='' />
                                    })}
                                </div>
                                <div className='why-haimawang-tags-row'>
                                    <div className='why-haimawang-tags-row-one' style={{ height: '89px' }}>参考<br />CSTA标准</div>
                                    <div className='why-haimawang-tags-row-m' style={{ background: '#54C2FB' }}>{`Level\xa01A-1B`}</div>
                                    <div className='why-haimawang-tags-row-m' style={{ background: '#7CD702' }}>{`Level\xa02`}</div>
                                    <div className='why-haimawang-tags-row-m' style={{ background: '#FFBD00' }}>{`Level\xa03A-3B`}</div>
                                </div>
                                <div className='why-haimawang-tags-row'>
                                    <div className='why-haimawang-tags-row-one' style={{ height: '188px' }}>课程内容</div>
                                    {
                                        whyHaimawangWord.map(item => {
                                            return <div key={item.id} className='why-haimawang-tags-row-b'
                                                style={{ background: item.background }}>{item.content}</div>
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* 优秀的在线辅导教学 */}
                    <div className="onlineTeaching">
                        <BallFloat style={{width: '20px',height: '20px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', right: '300px',top: '160px'}}></BallFloat>
                        <BallAniBox position={{'right': 0, 'top': '50px','flexDirection': 'row-reverse'}} bigPosition={{}} smallPosition={{'top': '50px','left': '-20px'}}></BallAniBox>
                        <TitleBox title={'优秀的在线辅导教学'} subTitle={'省去来回奔波的辛苦，让孩子在家也能学好编程课'} color={'#00104D'}/>
                        <div className="onlineTeaching-body">
                            <img className="background" src={`${process.env.PUBLIC_URL}/assets/teachingBg.png`} alt='' />
                            <img className="onlineTeaching-content" src={`${process.env.PUBLIC_URL}/assets/teachingCon.png`} alt='' />
                        </div>
                    </div>

                    {/* 优秀作品展示 */}
                    <div id="product" className="productsShow">
                        <TitleBox title={'优秀作品展示'} subTitle={'点击图片可体验'} color={'#00104D'}/>
                        <div className="productsShow-list">
                            <BallFloat style={{width: '35px',height: '35px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', left: '20px',top: '-120px'}}></BallFloat>
                            <BallFloat style={{width: '27px',height: '27px',background: 'linear-gradient(#70A7FE,#4271FF)', left: '120px',top: '-60px'}}></BallFloat>
                            <BallFloat style={{width: '20px',height: '20px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', left: '206px',top: '-100px'}}></BallFloat>
                            <BallFloat style={{width: '47px',height: '47px',background: 'linear-gradient(#D2E3FF,#A0B8FF)', right: '190px',top: '-144px'}}></BallFloat>
                            <BallFloat style={{width: '27px',height: '27px',background: 'linear-gradient(#70A7FE,#4271FF)', right: '190px',top: '-44px'}}></BallFloat>
                            {goodWorkList.map(item => {
                                return <div className="productsShow-box" key={item.id} onClick={() => this.toProductDetail(item.id)}>
                                    <img src={item.coverUrl} alt='' />
                                    <div className="productsShow-con">
                                        <div className="title">{item.name}</div>
                                        <div className="text">{item.intro}</div>
                                    </div>
                                </div>
                            })}
                            
                        </div>
                    </div>

                    {/* 编程体验课安排 */}
                    <div id="test" className="testCourse">
                        <BallFloat style={{width: '20px',height: '20px',background: 'linear-gradient(#EBCFFF,#E7B9FF)', left: '117px',top: '185px'}}></BallFloat>
                        <BallAniBox position={{'left': '10px', 'top': '100px'}} bigPosition={{}} smallPosition={{'top': '90px','left': '-100px'}}></BallAniBox>
                        <TitleBox title={'编程体验课安排'} subTitle={'6节课，完成零基础入门'} color={'#00104D'}/>
                        {/* 课时tab */}
                        <div className="testCourse-tab-box">
                            <div className="testCourse-tab">
                                <div className={checkCourse === 1 ? 'active' : ''} onClick={() => this.arrowRemove(1)}>第一课时</div>
                                <div className={checkCourse === 2 ? 'active' : ''} onClick={() => this.arrowRemove(2)}>第二课时</div>
                                <div className={checkCourse === 3 ? 'active' : ''} onClick={() => this.arrowRemove(3)}>第三课时</div>
                                <div className={checkCourse === 4 ? 'active' : ''} onClick={() => this.arrowRemove(4)}>第四课时</div>
                                <div className={checkCourse === 5 ? 'active' : ''} onClick={() => this.arrowRemove(5)}>第五课时</div>
                                <div className={checkCourse === 6 ? 'active' : ''} onClick={() => this.arrowRemove(6)}>第六课时</div>
                            </div>
                            <div className="testCourse-arrow">
                                <ArrowDiv className="arrow" arrowRemoveSize={arrowRemoveSize}></ArrowDiv>
                            </div>
                        </div>
                        {/* 课时内容 */}
                        <div className="testCourse-body">
                            <Carousel dots={false} ref={el=>{this.chooseIndex = el}}>
                                {testCourseData.map(item => {
                                    return <div className="testCourse-list" key={item.id}>
                                        <div className="left">
                                            <img src={`${process.env.PUBLIC_URL}/assets/courseImg${item.id}.png`} alt=""/>
                                        </div>
                                        <div className="right">
                                            <div className="title">LV0</div>
                                            <div className="title">{item.title}</div>
                                            <div className="con">
                                                <div>
                                                    <span className="circle"></span>
                                                    <b>涵盖科普内容：</b>{item.content}
                                                </div>
                                                <div>
                                                    <span className="circle"></span>
                                                    <b>编程知识点：</b>{item.codeKnowledge}
                                                </div>
                                                <div>
                                                    <span className="circle"></span>
                                                    <b>课程知识点：</b>
                                                    {item.courseKnowledge}
                                                </div>
                                            </div>
                                            {item.id === 0 ? <div className="clickBtn" onClick={() => this.showVideoFn(true)}>视频学习</div> : <div className="clickBtn" onClick={this.onClickToPay.bind(this)}>
                                                <img src={`${process.env.PUBLIC_URL}/assets/lock.png`} alt=""/>解锁视频    
                                            </div>}
                                            
                                        </div>
                                    </div>
                                })}
                                
                            </Carousel>
                        </div>
                    </div>
                    {/* 底部 */}
                    <div className="footer" style={{
                        background: `url(${process.env.PUBLIC_URL}/assets/background-bottom.png) no-repeat top`,
                        backgroundSize: 'auto 480px'
                    }}>
                        <div className='footer-content'>
                            <div className="question">
                                <div className="footer-title"><Icon type="question-circle" />{`\xa0常见问题`}</div>
                                <div className="question-list">
                                    <div>问题一：怎么上课？</div>
                                    <div>回答：在家使用电脑即可。建议下载谷歌浏览器，提升创作体验。</div>
                                </div>
                                <div className="question-list">
                                    <div>问题二：家长需要参与课程吗？</div>
                                    <div>回答：前1-2次课程，家长需要配合给孩子准备一下电脑浏览器。孩子的学习账号需要家长授权登录。</div>
                                </div>
                                <div className="question-list">
                                    <div>问题三：学习时间冲突，课程还可以回看嘛？</div>
                                    <div>回答：可以，课程为动画录播课，支持反复观看学习哦～ </div>
                                </div>
                            </div>
                            <div className="contactus">
                                <div className="footer-title"><Icon type="phone" />{`\xa0联系我们`}</div>
                                <div className="contact-body">
                                    <div className="contact-form">
                                        <div>
                                            <Input onFocus={() => this.setState({ phoneStatus: false })}
                                                onChange={(e) => this.setState({ phone: e.target.value })}
                                                maxLength={11}
                                                placeholder='请输入11位手机号'
                                                className='input-value-phone'
                                                prefix={<Icon type="mobile" style={phoneStatus ? { fontSize: '20px', color: 'red' } : { fontSize: '16px', color: 'rgba(0,0,0,0.25)' }} />}
                                            />
                                        </div>
                                        <div>
                                            <Input
                                                onFocus={() => this.setState({ nameStatus: false })}
                                                onChange={(e) => this.setState({ name: e.target.value })}
                                                maxLength={5}
                                                placeholder='孩子姓名'
                                                className='input-value-name'
                                                prefix={<Icon type="user" style={nameStatus ? { fontSize: '20px', color: 'red' } : { fontSize: '16px', color: 'rgba(0,0,0,0.25)' }} />} 
                                            />
                                        </div>
                                        <div>
                                            <Input
                                                onFocus={() => this.setState({ ageStatus: false })}
                                                onChange={(e) => this.setState({ age: e.target.value })}
                                                maxLength={3}
                                                placeholder='孩子年龄'
                                                className='input-value-age'
                                                prefix={<Icon type="align-left" style={ageStatus ? { fontSize: '20px', color: 'red' } : { fontSize: '16px', color: 'rgba(0,0,0,0.25)' }} />} 
                                            />
                                        </div>
                                        <div onClick={userInfoGather ? null : this.onClickSave} className={classNames({ 'for-get': true, 'for-success': userInfoGather })}>
                                            {userInfoGather ? '提交成功~' : (loading ?
                                                <Icon style={{ fontSize: '24px' }} type="loading" /> : '免费领取试听课')}
                                        </div>
                                    </div>
                                    <div className="contact-ercode">
                                        <img src={qrCodeUrl4Official} style={{ width: '175px', height: '175px', background: '#fff' }} alt='' />
                                        <div style={{ textAlign: 'left', fontSize: '13px', paddingTop: '10px' }}>
                                            关注海码王少儿编程<br />更多优惠不容错过
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='copyright'>
                            <div>海码王学生版、海码王教师版</div>
                            <div>Copyright © 2019 青帝教育科技（北京）有限公司版权所有. 京ICP备18037313号-1</div>
                            <div>服务热线：400-800-8540</div>
                            <div style={{height: '90px'}}></div>
                        </div>
                    </div>
                    <Modal
                        visible={visible}
                        footer={null}
                        onCancel={this.handleCancel}
                        destroyOnClose
                        closable={false}
                    >
                        <div className='modal-homepage-content'>
                            <div className='modal-homepage-content-title'>请选择孩子的上课时间</div>
                            <div className='modal-homepage-content-item'>
                                <div className='modal-homepage-content-item-left'>课程名称：</div>
                                <div style={{ width: '268px', padding: '4px 11px', borderRadius: '5px', border: '1px solid #54C3D4' }}>
                                    {coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0].name}</div>
                            </div>
                            <div className='modal-homepage-content-item'>
                                <div className='modal-homepage-content-item-left'>课程期次：</div>
                                <Select
                                    style={{ width: '268px' }}
                                    dropdownStyle={{ maxHeight: '120px', overflow: 'auto' }}
                                    showSearch
                                    placeholder="请选择课程期次"
                                    optionFilterProp="children"
                                    onChange={this.onChangeLesson}
                                    labelInValue
                                    filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {
                                        coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList.map(item => {
                                            return <Option key={item.id} value={item.id}>{`${item.issue}期`}</Option>
                                        })
                                    }
                                </Select>
                            </div>
                            <div className='modal-homepage-content-item'>
                                <div className='modal-homepage-content-item-left'>课程辅导时间：</div>
                                <div style={{ width: '268px', height: '31px', padding: '4px 11px', borderRadius: '5px', border: '1px solid #54C3D4' }}>
                                    {lessonInfo.teachStartAt &&
                                        `${moment(lessonInfo.teachStartAt).format('YYYY年MM月DD日')}~${moment(lessonInfo.teachEndAt).format('YYYY年MM月DD日')}`}
                                </div>
                            </div>
                            <div style={{ width: '100%', padding: '6px 0 0 139px', fontSize: '12px', color: '#999999' }}>在线辅导时间：周一至周五 9:00 - 22:00 </div>
                            <Button
                                onClick={this.onClickHasChoose}
                                className='modal-homepage-content-course-button'
                                type='primary'>选好了</Button>
                        </div>
                    </Modal>
                </div>
                {/* 悬浮底部的“课程购买”    已做好，阻挡视线，暂时隐藏*/}
                <div className="home-fixed-body" style={{display: "block"}}>
                    <div className="home-fixed-box">
                        <div className="home-fixed-con">
                            <div style={{margin: '0 40px 0 60px'}}>Scratch编程体验课</div>
                            <div className="home-fixed-original-price">{coursePackageInfoList[0] &&`原价${ coursePackageInfoList[0].courseInfoList[0].price}元`}</div>
                            <div className="home-fixed-truth-price">
                                <span>{coursePackageInfoList[0] && coursePackageInfoList[0].courseInfoList[0].realPrice}元</span>/6节课
                            </div>
                            <div style={{ fontSize: '12px' }}>（开课前无条件退款）</div>
                        </div>
                        <div onClick={this.onClickToPay.bind(this)} className="home-fixed-btn">课程购买</div>
                    </div>
                </div>
                {/* 侧导航 */}
                <Anchor className="home-side-menu" affix={false}>
                    <Link href="#haima" title="海码王" />
                    <Link href="#code" title="编程时代" />
                    <Link href="#teacher" title="师资团队" />
                    <Link href="#course" title="课程体系" />
                    <Link href="#product" title="作品展示" />
                    <Link href="#test" title="体验课程" />
                </Anchor>
                {/* 视频 */}
                { showvideoBox && <HomeVideo videoUrl={coursePackageLessonInfoList[0].videoUrl} showVideoFn={this.showVideoFn.bind(this)} showVideo={showVideo} showvideoBox={showvideoBox} showVideoPause={showVideoPause}  onClickToPay={this.onClickToPay.bind(this)}></HomeVideo>}
            </div>
        )
    }
}
export default Homepage;
const TitleBox = React.memo(
    ({title,subTitle,color}) => {
        return <div className="titleBox">
            <div className="title-main" style={{color}}>{title}</div>
            <div className="title-line"></div>
            <div className="title-sub" style={{color}}>{subTitle}</div>
        </div>
    }
)
const BallAniBox = React.memo(
    ({position,bigPosition,smallPosition}) => {
        return <div className="ballBox" style={{...position}}>
            <img src={`${process.env.PUBLIC_URL}/assets/bigball.png`} className="bigBall" style={{...bigPosition}} alt=''/>
            <img src={`${process.env.PUBLIC_URL}/assets/smallball.png`} className="smallBall" style={{...smallPosition}} alt=''/>
        </div>
    }
)
const BallFloat = React.memo(
    ({style}) => {
        return <div className="ballFloat" style={{...style}}></div>
    }
)