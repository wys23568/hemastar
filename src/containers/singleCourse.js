import React, { PureComponent } from 'react';
import { Card, Button, Icon, Modal, Rate } from 'antd';
import * as message from 'Message';
import styled from 'styled-components';
import configApi from 'Config/configApi.js';
import './singleCourse.less';
import { getCourseDetail, closeShowGradeBox, postGradeForTeacher, openShowGradeBox, openScroll } from 'Redux/actions/courseactions.js';
import { getCourseDetail as serviceGetCourseDetail } from 'Service/course.js';
import { connect } from 'react-redux';
import moment from 'moment';
import CourseItem from 'Components/course/courseItem.js';
import Myvideo from 'Components/Myvideo/Myvideo.js';
import Orderdistribution from 'Components/orderDistribution/orderDistribution.js';
export const Scratch = styled.div`
height: 28px;
color:#555555;
display:flex;
font-size:16px;
align-items:center;
`
@connect(({ course: { courseDetail, showGradeBox, courseDistribution, scroll }, login: { token } }) => ({ courseDetail, showGradeBox, courseDistribution, token, scroll }))
class SingleCourse extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                courseId: '',
                id: '',
                title: '',
                knowledges: '',
                studentCourseHomeworkInfo: { studyFlag: '' },
                coverUrl: '',
                videoUrl: '',
                lockFlag: 2,
                teacherCourseId: '',
                number: '',
                startAt: ''
            },
            visible: false,
            rateValue: 5,
            showCartoon: false,
            showCartoonBox: false
        }
    }
    componentDidMount() {
        const { dispatch, location, match, token } = this.props;
        let params = new URLSearchParams(location.search);
        dispatch(getCourseDetail({ courseId: match.params.mcid, teacherCourseId: params.get('teacherCourseId') }));
        dispatch(openScroll(true))
        this.payer = setInterval(() => {
            const { courseDetail } = this.props;
            let requestData;
            const dateNow = moment(new Date().getTime()).format('YYYYMMDD')
            courseDetail && courseDetail.courseLessonInfoList.forEach(item => {
                if (moment(new Date(Number(item.startAt)).getTime()).format('YYYYMMDD') === dateNow) {
                    requestData = item
                }
            })
            if (requestData && Number(requestData.startAt) < new Date().getTime() && requestData.lockFlag === 1) {
                serviceGetCourseDetail({ courseId: match.params.mcid, teacherCourseId: params.get('teacherCourseId'), loginToken: token }).then(function (response) {
                    if (response.data.code !== 200) {
                        // message.error(response.data.error, 1)
                        return
                    }
                    dispatch({ type: 'saveCourseDetail', courseDetail: response.data.data });
                })
                    .catch(function (error) {
                        console.log(error);
                    });

            }
        }, 3000)
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.courseDetail && this.props.scroll) {
            const { dispatch, courseDetail } = this.props
            let openArr = [];
            openArr = courseDetail.courseLessonInfoList.filter(item => item.lockFlag !== 1)
            openArr.length > 3 && openArr.length !== courseDetail.courseLessonInfoList.length &&
                this.scrollRef.scrollTo(0, 320 * (openArr.length - 1));
            dispatch(openScroll(false))

        }
    }
    componentWillUnmount() {
        clearInterval(this.payer);
    }
    courseClick = () => {
        const { dispatch } = this.props;
        dispatch(openShowGradeBox())
    }
    showModal = (data) => {
        document.body.style.overflow = 'hidden'
        this.setState({
            visible: true,
            data
        });
    }
    handleCancel = (e) => {
        document.body.style.overflow = 'auto'
        this.setState({
            visible: false,
        });
    }
    onClickToStudy = () => {
        const { courseId, id, studentCourseHomeworkInfo: { teacherCourseId } } = this.state.data;
        window.location.href = (`${configApi.jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`)
    }
    toStudyOrCreate = ({ state }, { courseId, id, studentCourseHomeworkInfo: { teacherCourseId } }) => {
        if (state > 1) {
            window.open(`${configApi.routerApi}#/personal-center/my-homework/${courseId}/${id}?courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`)
        }
        else {
            window.open(`${configApi.jumpApi}?type=2&courseId=${courseId}&courseLessonId=${id}&teacherCourseId=${teacherCourseId}`)
        }
    }
    closeGradeBox = () => {
        const { dispatch } = this.props;
        dispatch(closeShowGradeBox())
    }
    closeCartoonBox = () => {
        this.setState({ showCartoonBox: false })
    }
    onChangeRateValue = (value) => {
        this.setState({ rateValue: value })
    }
    onClickUploadGrade = (courseId, teacherCourseId) => {
        const { rateValue } = this.state;
        const { dispatch } = this.props;
        if (!rateValue) {
            message.info('我们的老师很辛苦，给个合适的评分呗~', 1)
            return
        }
        dispatch(postGradeForTeacher({ courseId, teacherCourseId, score: rateValue * 20 }))
    }
    onClickToBack = () => {
        const { history } = this.props;
        history.push('/my-course')
    }
    onClickReady = ({ studentScore, stopFlag }) => {
        if (!studentScore && stopFlag === 2) {
            this.courseClick()
            return
        }
        this.setState({ showCartoonBox: true })
    }
    render() {
        const { courseDetail, showGradeBox, courseDistribution } = this.props;
        const { data: { videoUrl },
            visible, rateValue, showCartoonBox } = this.state;
        const { qrCodeUrl4Official = '', qrCodeUrl4Teacher = '' } = courseDistribution;
        return (
            <div className='course'>
                {courseDetail && <div>
                    <Card style={{ borderRadius: '10px', padding: '0 20px' }}>
                        <div className='course-title'><Scratch>
                            <Icon type="left-circle" theme="filled" onClick={this.onClickToBack}
                                style={{ fontSize: '24px', marginRight: '9px', color: '#1a696a' }} />
                            {courseDetail.name}</Scratch>
                            {
                                !courseDetail.studentScore && <div
                                    onClick={this.onClickReady.bind(this, courseDetail)}
                                    className='course-title-before'
                                >
                                    {!courseDetail.studentScore && courseDetail.stopFlag === 2 ? '课程评价' : '课前准备'}
                                </div>
                            }
                        </div>
                        <div className='course-content' ref={el => this.scrollRef = el}>
                            {
                                courseDetail.courseLessonInfoList.map(item => {
                                    return <CourseItem key={item.id}
                                        data={item}
                                        showModal={this.showModal.bind(this)}
                                        toStudyOrCreate={this.toStudyOrCreate.bind(this)}
                                    />
                                })
                            }
                        </div>
                    </Card>
                </div>
                }
                {visible && <div onClick={this.handleCancel} className='show-background'>
                    <div onClick={(e) => { e.stopPropagation() }} className='show-video-content'>
                        <Myvideo videoUrl={videoUrl}
                            onClickToStudy={this.onClickToStudy.bind(this)}
                            handleCancel={this.handleCancel.bind(this)} />
                    </div>
                </div>
                }
                {courseDetail && <Modal
                    width={280}
                    visible={showGradeBox}
                    footer={null}
                    onCancel={this.closeGradeBox}
                    bodyStyle={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'column' }}
                >
                    <div className='grade-modal-title'>
                        <img
                            style={{ width: '20px', height: '20px' }}
                            src={`${process.env.PUBLIC_URL}/assets/icon-smile-grade.png`} alt='' />{`\xa0恭喜你学完全部课程！`}</div>
                    <div style={{ fontSize: '16px' }}>给辅导老师打个分吧！</div>
                    <div className='grade-modal-content'>我的评分：
                    <Rate allowClear={false} allowHalf onChange={this.onChangeRateValue} value={rateValue} /></div>
                    <Button
                        className='grade-modal-button'
                        type='primary'
                        onClick={this.onClickUploadGrade.bind(this, courseDetail.id, courseDetail.teacherCourseId)}>
                        上传评分
                    </Button>
                </Modal>}
                <Modal
                    width={820}
                    visible={showCartoonBox}
                    footer={null}
                    closable={false}
                    onCancel={this.closeCartoonBox}
                    bodyStyle={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'column' }}
                >
                    <div className='cartoon-box-title'>课前准备</div>
                    <div style={{ width: '100%', padding: '44px 94px' }}>
                        < Orderdistribution qrCodeUrl4Official={qrCodeUrl4Official} qrCodeUrl4Teacher={qrCodeUrl4Teacher} />
                    </div>
                </Modal>
            </div>
        )
    }
}
export default SingleCourse;