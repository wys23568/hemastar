import React, { PureComponent } from 'react';
import { Icon } from 'antd';
import * as message from 'Message';
import './myCourseList.less';
import { getCourseList } from 'Redux/actions/courseactions.js';
import { connect } from 'react-redux';
import classNames from 'classnames';
import moment from 'moment';
// Design thinking caused such garbage code . If possible, please discard it. 
@connect(({ course: { courseList } }) => ({ courseList }))
class MyCourseList extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            visible: false
        }
    }
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getCourseList({ pageNumber: '0', pageSize: '20', source: '1' }));
    }

    showModal = () => {
        document.body.style.overflow = 'hidden'
        this.setState({
            visible: true,
        });
    }
    handleCancel = () => {
        document.body.style.overflow = 'auto'
        this.setState({
            visible: false,
        });
    }
    onClickToCourse = (item) => {
        const { history } = this.props;
        if (item.buyFlag === 2) {
            if (item.courseInfo.state > 4) {
                history.push({
                    pathname: `/my-course/${item.courseInfo.id}`,
                    search: `?teacherCourseId=${item.courseInfo.teacherCourseId}`
                })
            }
            else {
                message.info('此课程尚未开课，请再等待一下吧~', 1)
            }
        }
        else {
            this.showModal()
        }
    }
    render() {
        const gbgMap = new Map([[0, { left: '185px', top: '320px' }], [1, { left: '641px', top: '354px' }],
        [2, { left: '416px', top: '548px' }], [3, { left: '39px', top: '691px' }], [4, { left: '824px', top: '758px' }],
        [5, { left: '491px', top: '935px' }], [6, { left: '106px', top: '1065px' }]])
        const { visible } = this.state;
        const { courseList } = this.props;
        const { coursePackageInfoList = [] } = courseList;
        return (
            <div className='course-list'
                style={{
                    background: `url(${process.env.PUBLIC_URL}/assets/gbg/gbg-background.png) no-repeat top`,
                    backgroundSize: 'cover'
                }}>
                <img
                    style={{ width: '206px', height: '200px' }}
                    src={`${process.env.PUBLIC_URL}/assets/gbg/gbg-title.png`}
                    alt='' />
                {
                    coursePackageInfoList.map((item, index) => {
                        return <div
                            onClick={this.onClickToCourse.bind(this, item)}
                            key={item.id}
                            style={{ position: 'absolute', cursor: 'pointer', ...gbgMap.get(index) }}>
                            <img style={{ width: '366px' }}
                                className={classNames({ gray: item.buyFlag !== 2 })}
                                src={`${process.env.PUBLIC_URL}/assets/gbg/gbg-course-${index}.png`}
                                alt='' />
                            {
                                item.buyFlag === 2 && item.courseInfo.state !== 6 &&
                                <div className='course-list-time'>
                                    {`${moment(item.courseInfo.teachStartAt).format("YYYY.MM.DD")} - ${moment(item.courseInfo.teachEndAt).format("YYYY.MM.DD")}`}
                                </div>
                            }
                            {
                                item.buyFlag === 2 && item.courseInfo.state > 4 && <img
                                    className='course-list-haima'
                                    src={`${process.env.PUBLIC_URL}/assets/gbg/gbg-course-haima.png`}
                                    alt='' />
                            }
                        </div>
                    })
                }
                {
                    visible && <div onClick={this.handleCancel} className='cousrse-modal'>
                        <div
                            onClick={(e) => { e.stopPropagation() }}
                            className='cousrse-modal-content'
                            style={{
                                background: `url(${process.env.PUBLIC_URL}/assets/gbg/gbg-message.png) no-repeat top`,
                                backgroundSize: 'cover'
                            }}>
                            <Icon
                                onClick={this.handleCancel}
                                type="close-circle"
                                style={{ fontSize: '51px', color: '#c0bfbc' }} />
                        </div>
                    </div>
                }
            </div>
        )
    }
}
export default MyCourseList;