let configApi = {
};
let dN;
let confirmArg = {};
let appid;
if (window.newDNS && window.location.href.includes(window.newDNS)) {
    dN = window.newDNS;
    confirmArg = {
        appId: '20111119',
        appKey: '6cf1sdf6ee654c231a584eb382d32ab7'
    }
    appid = 'wx02edbe1c9bdace2d'
}
else {
    dN = window.defaultDNS
    confirmArg = {
        appId: '20111113',
        appKey: 'ab731a584eb3826cf1sdf6ee654c2d32'
    }
    appid = 'wx2b8b6b3771f9a10e'
}
if (window.location.href.includes('/test/HaimaStudent/')) {
    //测试及本地开发配置
    configApi = {
        ...configApi,
        jumpApi: `${dN}/test/HaimaStudent/scratch/index.html`,
        requestApi: `${dN}/test/api`,
        routerApi: `${dN}/test/HaimaStudent/student/index.html`,
        confirmArg,
        appid
    }
}
else {
    //正式配置
    configApi = {
        ...configApi,
        jumpApi: `${dN}/s/scratch/index.html`,
        requestApi: `${dN}/api`,
        routerApi: `${dN}/s/student/index.html`,
        confirmArg,
        appid
    }
}
export default configApi;