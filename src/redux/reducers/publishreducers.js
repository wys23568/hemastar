const initialState = {
    publishDetail: {}
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'savePublishDetail':
            return {
                ...state,
                publishDetail: action.publishDetail
            }
        case 'saveIntro':
            return {
                ...state,
                publishDetail: {
                    ...state.publishDetail,
                    intro: action.intro
                }
            }
        case 'saveTagIds':
            return {
                ...state,
                publishDetail: {
                    ...state.publishDetail,
                    tagIds: action.tagIds
                }
            }
        case 'saveOpenFlag':
            return {
                ...state,
                publishDetail: {
                    ...state.publishDetail,
                    openFlag: action.openFlag
                }
            }
        case 'saveOperateExplain':
            return {
                ...state,
                publishDetail: {
                    ...state.publishDetail,
                    operateExplain: action.operateExplain
                }
            }
        default:
            return state
    }
}