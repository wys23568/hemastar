const initialState = {
    homepageInfo:{},
    userInfoGather: false,
    homeCourseDetail: {},
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveHomepageInfo':
            return{
                ...state,
                homepageInfo:action.homepageInfo
            }
        case 'saveUserInfoGather':
            return {
                ...state,
                userInfoGather: action.userInfoGather
            }
        case 'saveHomeCourseDetail':
            return {
                ...state,
                homeCourseDetail: action.homeCourseDetail
            }
        default:
            return state
    }
}