const initialState = {
    notificationList: {}
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveNotificationList':
            return {
                ...state,
                notificationList: action.notificationList
            }
        default:
            return state
    }
}