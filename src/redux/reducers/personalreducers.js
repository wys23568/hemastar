const initialState = {
    myWorksList: {},
    myHomeworksList: {},
    myCollectionList: {},
    myLikeList: {},
    myOrderList: {},
    personalInfo: {},
    newOrOldType: 0,
    courseHomeworkList: {}
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveMyWorksList':
            return {
                ...state,
                myWorksList: action.myWorksList
            }
        case 'saveMyHomeworksList':
            return {
                ...state,
                myHomeworksList: action.myHomeworksList
            }
        case 'saveMyCollectionList':
            return {
                ...state,
                myCollectionList: action.myCollectionList
            }
        case 'saveMyLikeList':
            return {
                ...state,
                myLikeList: action.myLikeList
            }
        case 'saveMyOrderList':
            return {
                ...state,
                myOrderList: action.myOrderList
            }
        case 'savePersonalInfo':
            return {
                ...state,
                personalInfo: action.personalInfo
            }
        case 'updateAvatarUrl':
            return {
                ...state,
                personalInfo: {
                    ...state.personalInfo,
                    avatarUrl: action.avatarUrl
                }
            }
        case 'updateName':
            return {
                ...state,
                personalInfo: {
                    ...state.personalInfo,
                    name: action.name
                }
            }
        case 'updateGender':
            return {
                ...state,
                personalInfo: {
                    ...state.personalInfo,
                    gender: action.gender
                }
            }
        case 'phoneUnbindingComfirm':
            return {
                ...state, newOrOldType: action.newOrOldType
            }
        case 'saveCourseHomeworkList':
            return {
                ...state,
                courseHomeworkList: action.courseHomeworkList
            }
        default:
            return state
    }
}
