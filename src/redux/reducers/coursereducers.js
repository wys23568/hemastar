const initialState = {
    courseList: {},
    showGradeBox: false,
    courseDetail: null,
    courseDistribution: {},
    scroll: false
}
export default (state = initialState, action) => {
    switch (action.type) {
        case 'saveCourseList':
            return {
                ...state,
                courseList: action.courseList
            }
        case 'saveCourseDetail':
            return {
                ...state,
                courseDetail: action.courseDetail
            }
        case 'shouldShowGradeBox':
            return {
                ...state,
                showGradeBox: action.showGradeBox
            }
        case 'saveCourseDistribution':
            return {
                ...state,
                courseDistribution: action.courseDistribution
            }
        case 'shouldOpenScroll':
            return {
                ...state,
                scroll: action.scroll
            }
        default:
            return state
    }
}