import * as publishService from 'Service/publish.js';
import * as message from 'Message';
export const getPublishDetail = (data) => (dispatch, getState) => {
    const { token } = getState().login
    publishService.getPublishDetail({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'savePublishDetail', publishDetail: response.data.data ? response.data.data : {} })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const changeIntro = (intro) => (dispatch, getState) => {
    dispatch({ type: 'saveIntro', intro })
}
export const changeOperateExplain = (operateExplain) => (dispatch, getState) => {
    dispatch({ type: 'saveOperateExplain', operateExplain })
}
export const changeTagIds = (tagIds) => (dispatch, getState) => {
    dispatch({ type: 'saveTagIds', tagIds })
}
export const changeOpenFlag = (openFlag) => (dispatch, getState) => {
    dispatch({ type: 'saveOpenFlag', openFlag })
}
