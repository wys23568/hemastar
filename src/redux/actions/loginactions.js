import * as loginService from 'Service/login.js';
import * as message from 'Message';
export const showLoginModal = (visible) => (dispatch, getState) => {
    dispatch({ type: 'showLoginModal', visible })
}
export const deleteLoginInfo = (data = {}) => (dispatch, getState) => {
    dispatch({ type: 'deleteLoginInfo' })
}
export const getUserInfo = (token, data = {}) => (dispatch, getState) => {
    loginService.getUserInfo({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error('认证过期，请重新登陆', 1)
            dispatch(deleteLoginInfo())
            window.localStorage.removeItem('h_token')
            dispatch(showLoginModal(true))
            return
        }
        dispatch({ type: 'saveUserInfo', userInfo: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const saveLoginToken = (token, data = {}) => (dispatch, getState) => {
    window.localStorage.setItem('h_token', token);
    dispatch({ type: 'saveLoginToken', token })
    dispatch(getUserInfo(token, data))
}
export const saveSsoInfo = (ssoInfo) => (dispatch, getState) => {
    dispatch({ type: 'saveSsoInfo', ssoInfo })
    dispatch(showLoginModal(true))
}
export const weixinLogin = (code) => (dispatch, getState) => {
    const accountType = 'student';
    loginService.weixinLogin({ code, accountType }).then(function (response) {
        if (response.data.code !== 200) {
            message.error('登陆失败，请重新登陆', 1)
            dispatch(deleteLoginInfo())
            window.localStorage.removeItem('h_token')
            dispatch(showLoginModal(true))
            return
        }
        response.data.data.loginToken ? dispatch(saveLoginToken(response.data.data.loginToken)) : dispatch(saveSsoInfo(response.data.data.sso))
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const weixinLoginForCode = (data) => (dispatch, getState) => {
    loginService.weixinLoginForCode(data).then(function (response) {
        if (response.data.code !== 200) {
            message.error(data.data.error, 1)
            return
        }
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const weixinLoginConfirm = (data) => (dispatch, getState) => {
    loginService.weixinLoginConfirm(data).then(function (response) {
        if (response.data.code !== 200) {
            message.error('验证码输入错误', 1)
            return
        }
        dispatch(saveLoginToken(response.data.data.loginToken, { avatarUrl: data.avatarUrl, name: data.name, useFlag: 2 }))
        dispatch(showLoginModal(false))
    })
        .catch(function (error) {
            console.log(error);
        });
}