import * as reportService from 'Service/report.js';
import * as message from 'Message';
export const postReport = (data) => (dispatch, getState) => {
    const { token } = getState().login
    reportService.postReport({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        message.success('举报成功')
    })
        .catch(function (error) {
            console.log(error);
        });
}