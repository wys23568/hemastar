import * as paymentService from 'Service/payment.js';
import * as message from 'Message';
export const createOrder = (data) => (dispatch, getState) => {
    const { token } = getState().login
    paymentService.createOrder({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveOrderInfo', orderInfo: response.data.data })
        dispatch({ type: 'showOrClosePayModal', payVisible: true })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const continuePaying = (data) => (dispatch, getState) => {
    const { token } = getState().login
    paymentService.continuePaying({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveOrderInfo', orderInfo: response.data.data })
        dispatch({ type: 'showOrClosePayModal', payVisible: true })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const showOrClosePayModal = (payVisible) => (dispatch, getState) => {
    dispatch({ type: 'showOrClosePayModal', payVisible })
}
export const getOrderDetail = (data) => (dispatch, getState) => {
    const { token } = getState().login
    paymentService.getOrderDetail({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveOrderDetail', orderDetail: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}
export const getOrderDistribution = (data) => (dispatch, getState) => {
    const { token } = getState().login
    paymentService.getOrderDistribution({ ...data, loginToken: token }).then(function (response) {
        if (response.data.code !== 200) {
            message.error(response.data.error, 1)
            return
        }
        dispatch({ type: 'saveOrderDistribution', orderDistribution: response.data.data })
    })
        .catch(function (error) {
            console.log(error);
        });
}