import Axios from './axios.js';
export const getCourseList = async (data) => {
    return Axios.post('/haimawang/account/student/course/list',data)
}
export const getCourseDetail = async (data) => {
    return Axios.post('/haimawang/account/student/course/detail',data)
}
export const postGradeForTeacher = async (data) => {
    return Axios.post('/haimawang/account/student/course/grade',data)
}
export const getCourseDistribution = async (data) => {
    return Axios.post('/haimawang/account/student/course/order/finish', data)
}
