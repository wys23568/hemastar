import Axios from './axios.js'
export const getWorksList = async (data) => {
    return Axios.post('/haimawang/production/list', data)
}
export const getWorksDetail = async (data) => {
    return Axios.post('/haimawang/production/detail', data)
}
export const doLike = async (data) => {
    return Axios.post('/haimawang/account/student/production/like/add', data)
}
export const cancelLike = async (data) => {
    return Axios.post('/haimawang/account/student/production/like/delete', data)
}
export const doCollected = async (data) => {
    return Axios.post('/haimawang/account/student/production/collect/add', data)
}
export const cancelCollected = async (data) => {
    return Axios.post('/haimawang/account/student/production/collect/delete', data)
}
export const getProductionTag = async () => {
    return Axios.get('/haimawang/config/productionTag')
}
export const getProductionCover = async () => {
    return Axios.get('/haimawang/config/productionCover')
}
export const postPublishInfo = async (data) => {
    return Axios.post('/haimawang/account/student/production/publish', data)
}
export const getCommentList = async (data) => {
    return Axios.post('/haimawang/account/student/production/comment/list', data)
}
export const getReplyList = async (data) => {
    return Axios.post('/haimawang/account/student/production/commentReply/list', data)
}
export const postComment = async (data) => {
    return Axios.post('/haimawang/account/student/production/comment/add', data)
}
export const postReply = async (data) => {
    return Axios.post('/haimawang/account/student/production/commentReply/add', data)
}
export const doCommentLike = async (data) => {
    return Axios.post('/haimawang/account/student/production/comment/like/add', data)
}
export const cancelCommentLike = async (data) => {
    return Axios.post('/haimawang/account/student/production/comment/like/delete', data)
}
export const doReplyLike = async (data) => {
    return Axios.post('/haimawang/account/student/production/commentReply/like/add', data)
}
export const cancelReplyLike = async (data) => {
    return Axios.post('/haimawang/account/student/production/commentReply/like/delete', data)
}
export const deleteComment = async (data) => {
    return Axios.post('/haimawang/account/student/production/comment/delete', data)
}
export const deleteReply = async (data) => {
    return Axios.post('/haimawang/account/student/production/commentReply/delete', data)
}