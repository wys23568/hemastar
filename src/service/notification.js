import Axios from './axios.js'
export const getNotificationList=async(data)=>{
    return Axios.post('/haimawang/account/student/notify/list',data)
}
export const hasRead=async(data)=>{
    return Axios.post('/haimawang/account/student/notify/update',data)
}
export const readyToDelete=async(data)=>{
    return Axios.post('/haimawang/account/student/notify/delete',data)
}