import Axios from './axios.js'
export const getUserInfo = async (data) => {
    return Axios.post('/haimawang/account/student/info', data)
}
export const weixinLogin = async (data) => {
    return Axios.post('/service/account/common/sso/weixin/login', data)
}
export const weixinLoginForCode = async (data) => {
    return Axios.post('/service/account/common/sso/binding/request', data)
}
export const weixinLoginConfirm = async (data) => {
    return Axios.post('/service/account/common/sso/binding/confirm', data)
}