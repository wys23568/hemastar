import Axios from './axios.js'
export const getHomepageInfo = async (data) => {
    return Axios.post('/haimawang/config/homeConfig',data)
}
export const getUserInfoGather = async (data) => {
    return Axios.post('/haimawang/userInfoGather/save', data)
}
export const getCourseDetail = async (data) => {
    return Axios.post('/haimawang/course/package/detail', data)
}
