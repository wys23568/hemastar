import React, { useState, useEffect, useRef } from 'react';
import './homeVideo.less';
// import Hls from 'hls.js'
const VideoComponent = React.memo(
    props => {
        const { showVideoFn,onClickToPay,videoUrl } = props;
        const [media, setMedia] = useState(null)
        const [showVideo, setShowVideo] = useState(true)
        const [play, setPlay] = useState(false)
        const [showVideoPause, setShowVideoPause] = useState(false)
        const preMedia = usePrevious(media)
        useEffect(() => {
          var video = document.getElementById('media');
          if (window.Hls.isSupported()) {
              var hls = new window.Hls();
              hls.loadSource(videoUrl);
              hls.attachMedia(video);
          }
        }, [videoUrl])
        useEffect(() => {
            if (!preMedia && media) {
                playVideo()
            }
        })
        const ontimeupdate = (e) => {
            if(e.target.currentTime >= 106) {
                media.pause();
                media.currentTime = 106;
                // setShowVideo(false)
                setShowVideoPause(true)
            }
        }
        const hideVideoBox = () => {
            setShowVideo(false)
            setShowVideoPause(false)
            showVideoFn(false)
        }
        const toPay = () => {
            setShowVideo(false)
            setShowVideoPause(false)
            showVideoFn(false)
            onClickToPay()
        }
        const playVideo = () => {
            if (play) {
                setPlay(false)
                media.pause()
                return
            }
            else {
                setPlay(true)
                media.play()
            }
        }
        return <div className="videoBody">
            <div className="videoBox" onClick={hideVideoBox}></div>
            {showVideo && <video id="media" src={videoUrl} controls onTimeUpdate={ontimeupdate} ref={el => setMedia(el)}>
                您的浏览器不支持 video 标签。
            </video>}
            {showVideoPause && <div className="videoPauseBackground">
                <img src={`${process.env.PUBLIC_URL}/assets/play.png`} alt="" style={{width: '54px',height: '54px'}}/>
                <div className="toPay" onClick={toPay}>点击购买</div>
                <div>解锁完整视频</div>
            </div>}
        </div>
    }
)

export default VideoComponent;
function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}