import React from 'react';
import moment from 'moment';
import { Icon, Button, Row } from 'antd';
import * as message from 'Message';
import './courseItem.less';
const CourseItem = React.memo(
    ({ data = {}, showModal, toStudyOrCreate }) => {
        const { coverUrl, lockFlag, startAt, number, title, knowledges, studentCourseHomeworkInfo } = data;
        const onClickOpen = (itemData) => {
            showModal(itemData)
        }
        return (
            <div className='course-item'>
                <div className='course-item-img'>
                    <img
                        style={{ width: '100%', height: '100%', borderRadius: '10px' }}
                        src={lockFlag === 1 ? `${process.env.PUBLIC_URL}/assets/course-item-cover.png` : coverUrl} alt='' />
                    {
                        lockFlag === 1 ? <div className='course-item-img-lock' >
                            <img
                                onClick={() => { message.info('您的课程还未到开放时间呢~') }}
                                style={{ width: '66px', height: '66px', marginBottom: '8px', cursor: 'pointer' }}
                                src={`${process.env.PUBLIC_URL}/assets/icon-lock.svg`} alt='' />
                            <div style={{ textAlign: 'center' }}>
                                {moment(startAt).format('YYYY年MM月DD日\xa0HH:mm')}
                            </div>
                            <div className='lock-tips' style={{
                                background: `url(${process.env.PUBLIC_URL}/assets/course-item-tips.png) no-repeat`,
                                backgroundSize: '100% 100%'
                            }}  >待开放</div>
                        </div> :
                            <div className='course-item-img-lock' >
                                <Icon style={{ fontSize: '70px' }} onClick={onClickOpen.bind(this, data)} type="play-circle" />
                            </div>
                    }
                </div>
                <div className='course-item-content'>
                    <Row>
                        <div className='change-row1'>
                            <div style={{ marginRight: '28px' }}>
                                {`第${number}课时`}
                            </div>
                            {title}
                        </div>
                        <div className='change-row2'>课程知识点：</div>
                        <div className='change-row3'>
                            {
                                knowledges && knowledges.split('<br/>').map((item, index) => {
                                    return <div style={{ fontFamily: 'PingFangSC-Regular' }} key={index}>{item}</div>
                                })

                            }
                        </div>
                    </Row>
                    <Row type='flex' justify='end' align='middle'>
                        <Button
                            disabled={lockFlag === 1}
                            onClick={() => toStudyOrCreate(studentCourseHomeworkInfo, data)}
                            type='primary'
                            style={{ minWidth: '136px', height: '38px', borderRadius: '38px', }}>
                            {studentCourseHomeworkInfo && studentCourseHomeworkInfo.state > 1 ? '查看作业作品' : '开始学习'}
                        </Button>
                    </Row>
                </div>

            </div>
        )
    }
)
export default CourseItem;