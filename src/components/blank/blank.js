import React from 'react';
import './blank.less';
const Blank = ({ message = '' }) => {
    return (
        <div className='user-blank'>
            <div className='course-blank'>
                <img
                    style={{ width: '212px', height: '170px' }}
                    src={`${process.env.PUBLIC_URL}/assets/nocourse.png`}
                    alt=''
                />
                <div className='course-blank-div'>{message}</div>
            </div>
        </div>
    )
}
export default Blank;