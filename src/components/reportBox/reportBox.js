import React, { useEffect, useState } from 'react';
import './reportWorks.less';
import { getReportList } from 'Service/report.js'
import { Row, Select, Input, Button, Radio } from 'antd';
import * as message from 'Message';
import {
    withRouter
} from "react-router-dom";
const Option = Select.Option;
const { TextArea } = Input;
const RadioGroup = Radio.Group;
const ReportBox = React.memo(
    withRouter(
        (props) => {
            const { reportType,defendantId, reportSubmit } = props;
            const [selectedValue, setSelectedValue] = useState(null)
            const [textValue, setTextValue] = useState('')
            const [reportWorksList, setReportWorksList] = useState([])
            const [reportCommentList, setReportCommentList] = useState([])
            useEffect(() => {
                getReportList().then(function (response) {
                    if (response.data.code !== 200) {
                        message.error(response.data.error, 1)
                        return
                    }
                    setReportWorksList(response.data.data.productionKeyword);
                    setReportCommentList(response.data.data.commentKeyword)
                })
                    .catch(function (error) {
                        console.log(error);
                    });
            }, [])
            const onSelect = (value) => {
                setSelectedValue(value)
            }
            const onChange = (e) => {
                setTextValue(e.target.value)
            }
            const onChoose = (e) => {
                let option;
                for (let item of reportCommentList) {
                    if (item.id === e.target.value) {
                        option = { key: e.target.value, label: item.content }
                        setSelectedValue(option)
                        return
                    }
                }
               
            }
            const handleSubmit = () => {
                if (!selectedValue) {
                    message.info('请选择举报关键词')
                    return
                }
                const { key: reportConfigId, label: title } = selectedValue;
                reportSubmit({ type:reportType, defendantId, reportConfigId, title, content:textValue })
            }
            return (
                <div className='report-works'>
                    <Row className='report-works-row1'>{reportType!== 1 ? '举报此言论' : '举报该作品'}</Row>
                    {reportType !== 1 ? <RadioGroup
                        onChange={onChoose}
                        className='radio-group'
                        name="radiogroup"
                        defaultValue={7}>
                        {
                            reportCommentList.map(item => {
                                return <Radio className='radio' key={item.id} value={item.id}>{item.content}</Radio>
                            })
                        }
                    </RadioGroup>
                        :
                        <div><Row className='parent'>
                            <div className='stable'>举报关键词</div>
                            <Select
                                labelInValue
                                placeholder='请选择'
                                onSelect={onSelect}
                                className='change'
                            >{
                                    reportWorksList.map(item => {
                                        return <Option key={item.id} value={item.id}>{item.content}</Option>
                                    })
                                }</Select>
                        </Row>
                            <Row className='report-works-row3'><TextArea
                                onChange={onChange}
                                maxLength={140}
                                placeholder='请补充更多举报线索'
                                rows={6}
                                style={{ background: '#F9F9F9' }} /></Row></div>}
                    <Row style={{ marginTop: '24px' }}><Button type='primary' onClick={handleSubmit}
                        style={{ width: '126px', height: '38px', borderRadius: '38px' }}>举报</Button></Row>
                </div>
            )
        }
    )
)
export default ReportBox;