import React from 'react';
import './orderDistribution.less'
const Orderdistribution = React.memo(
    ({ qrCodeUrl4Teacher='', qrCodeUrl4Official='' }) => {
        return (
            <div className='pay-success-content-parent'>
                <div className='pay-success-content-child'>
                    <img style={{ width: '90px' }}
                        src={`${process.env.PUBLIC_URL}/assets/icon-first.png`}
                        alt='' />
                    <div className='content-child-top'>
                        添加海码老师
                </div>
                    <img
                        style={{ width: '170px', height: '170px' }}
                        src={qrCodeUrl4Teacher}
                        alt='' />
                    <div className='content-child-buttom'>
                        微信扫一扫
                </div>
                    <div className='content-child-word' >
                        <div>老师会指导您做好孩子的 </div>
                        <span style={{ color: '#54C3D4' }}>课前准备</span>并邀请您<span style={{ color: '#54C3D4' }}>进入学习群</span>
                    </div>
                </div>
                <div className='pay-success-content-child'>
                    <img style={{ width: '90px' }}
                        src={`${process.env.PUBLIC_URL}/assets/icon-second.png`}
                        alt='' />
                    <div className='content-child-top'>
                        关注海码王编程
                </div>
                    <img
                        style={{ width: '170px', height: '170px' }}
                        src={qrCodeUrl4Official}
                        alt='' />
                    <div className='content-child-buttom'>
                        微信扫一扫
                </div>
                    <div className='content-child-word' >
                        <div>第一时间获取</div>
                    <span style={{ color: '#54C3D4' }}>作业批改、上课提醒</span>等通知
                </div>
                </div>
            </div>
        )
    }
)
export default Orderdistribution;